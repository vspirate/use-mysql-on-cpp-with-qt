#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "src/mimetext.h"
#include "src/smtpclient.h"// send emails
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_AddCustomer_clicked()
{
    // data from inputs
    QString* name_of_product = new QString;
    *name_of_product = ui->name->text();

    QString* customer = new QString;
    *customer = ui->customer->text();

    QString* email = new QString;
    *email = ui->email->text();

    QString* link = new QString;
    *link = ui->link->text();

    // checking all inputs
    // if true delete pointers and create warning MessageBox
    if (*name_of_product == "" || *customer == "" || *email == "" || *link == "") {
        FUNCTIONS::deleteAll(name_of_product, customer, email, link);
        FUNCTIONS::createMSBWarning("Fill all inputs!");
        return;
    } if(!email->contains('@')){
        FUNCTIONS::deleteAll(name_of_product, customer, email, link);
        FUNCTIONS::createMSBWarning("Please fill email input");
        return;
    }

    // DB
    QSqlQuery query;

    // new command to use DB
    QString strCommand = "USE translate";
    if (!query.exec(strCommand)) {
        qDebug() << "Cannot use translate DataBase: " << query.lastError();
        FUNCTIONS::deleteAll(name_of_product, customer, email, link);
        return;
    } else
        qDebug() << "Sucessful USE translate";

    // checking for identity
    strCommand = "SELECT * FROM customers;";
    query.exec(strCommand);
    while (query.next()) {
        // if emails indentical, it will ended
        if (*email == query.value(2).toString()) {
            FUNCTIONS::deleteAll(name_of_product, customer, email, link);
            FUNCTIONS::createMSBWarning("This customer is already on DB");
            return;
        }
    }
    // command for fild (insert data to) DB
    strCommand = "INSERT INTO customers (name_of_product, name, email, link, isSened) VALUES('%1', '%2', '%3', '%4', 0);";

    // put arguments from inputs in SQL command
    QString str = strCommand.arg(*name_of_product).arg(*customer).arg(*email).arg(*link);
    if (!query.exec(str)) {
        qDebug() << "Cannot insert data to customers Table: " << query.lastError();
        FUNCTIONS::deleteAll(name_of_product, customer, email, link);
        return;
    } else {
        // clearing all inputs and space
        // refreshing view in table
        qDebug() << "Sucessful INSERT INTO translate";
        clearInputs();
        FUNCTIONS::deleteAll(name_of_product, customer, email, link);
        MainWindow::refreshTable();
        return;
    }
}

// button for function that we created before to view data from DB
void MainWindow::on_Update_clicked()
{
    MainWindow::refreshTable();
}

void MainWindow::on_SendEmails_clicked()
{
    // First we need to create an SmtpClient object
    // We will use the Gmail's smtp server (smtp.gmail.com, port 465, ssl)
    SmtpClient smtp("smtp.gmail.com", 465, SmtpClient::SslConnection);

    // We need to set the username (your email address) and the password
    // for smtp authentification.
    smtp.setUser("@gmail.com");
    smtp.setPassword("password");

    // DB
    QSqlQuery query;
    query.exec("USE translate;");

    // take data from table where we didn`t send email yet
    query.exec("SELECT * FROM customers WHERE isSened = 0;");
    while (query.next()) { 
          QString name_of_product = query.value(0).toString();
          QString name = query.value(1).toString();
          QString email = query.value(2).toString();

          // Now we create a MimeMessage object. This will be the email.
          MimeMessage message;

          message.setSender(new EmailAddress("@gmail.com", "Vlad"));
          message.addRecipient(new EmailAddress(email));
          message.setSubject("Cooperation with you");

          // Now add some text to the email.
          // First we create a MimeText object.
          MimeText text;

          text.setText(FUNCTIONS::getText(name, name_of_product));

          // Now add it to the mail
          message.addPart(&text);

          // Now we can send the mail
          smtp.connectToHost();
          smtp.login();
          smtp.sendMail(message);
          smtp.quit();
      }
    query.exec("UPDATE Customers SET isSened = 1 WHERE isSened = 0;");

}
