#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    w.setFixedSize(441, 421);

    if (!FUNCTIONS::createConnection()) {
        qDebug() << "Cannot connect to DB";
    } else {
        qDebug() << "Successful connect to DB";
    }

    w.refreshTable();
    return a.exec();

}

