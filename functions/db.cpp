#include "mainwindow.h"
#include "ui_mainwindow.h"

bool FUNCTIONS::createConnection()
{
    // for your MySQL table change this arguments
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setDatabaseName("translate");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("root");
    db.setPort(3306);
    if (!db.open()){
        qDebug() << "Cannot open database" << db.lastError();
        return false;
    }
    return true;
}

// refresh table view in app
bool MainWindow::refreshTable()
{
    // DB
    QSqlQuery query;
    QSqlQueryModel* model = new QSqlQueryModel();

    // new command to use DB
    QString strCommand = "USE translate";
    if (!query.exec(strCommand)) {
        qDebug() << "Cannot use translate DataBase: " << query.lastError();
        return false;
    } else {
        qDebug() << "Sucessful USE translate";
    }

    strCommand = "SELECT * FROM customers;";
    if(!query.exec(strCommand)) {
        qDebug() << "Cannot select data from customers: " << query.lastError();
        return false;
    } else {
        qDebug() << "Selected data from customers";
    }

    model->setQuery(query);
    ui->tableView->setModel(model);

    return true;
}
